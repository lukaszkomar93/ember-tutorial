Todos.TodosController = Ember.ArrayController.extend({
  actions: {
    createTodo: function() {
      // ToDo title is set by "New Todo" text field
      var title = this.get('newTitle');
      if (!title.trim()) { return; }

      // Create new Todo model
      var todo = this.store.createRecord('todo', {
        title: title,
        isCompleted: false
      });

      // Clear the text entry field
      this.set('newTitle', '');

      // Save new model
      todo.save();
    }
  },
  remaining: function() {
    return this.filterBy('isCompleted', false).get('length');
  }.property('@each.isCompleted'),
  inflection: function() {
    var remaining = this.get('remaining');
    return remaining === 1 ? 'item': 'items';
  }.property('remaining')
});
